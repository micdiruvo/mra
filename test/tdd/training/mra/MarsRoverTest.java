package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testShouldReturnObstacles() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		assertEquals(true,marsRover.planetContainsObstacleAt(2,3));
		
	}
	
	@Test
	public void testShouldReturnRoverPosition() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		String command="";
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		assertEquals("(0,0,N)",marsRover.executeCommand(command));
		
	}
	
	@Test
	public void testShouldReturnRoverPositionEast() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		String command="r";
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		assertEquals("(0,0,E)",marsRover.executeCommand(command));
		
	}
	
	@Test
	public void testShouldReturnRoverPositionWest() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		String command="i";
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		assertEquals("(0,0,W)",marsRover.executeCommand(command));
		
	}
	
	@Test
	public void testShouldReturnRoverPositionMovingForward() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		String command="f";
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		assertEquals("(7,7,N)",marsRover.executeCommand(command));
		
	}
	
	@Test
	public void testShouldReturnRoverPositionMovingBackward() throws MarsRoverException {
		int planetX=10;
		int planetY=10;
		String command="b";
		List<String> planetObstacles= new ArrayList<String>();
		planetObstacles.add("(4,7)");
		planetObstacles.add("(2,3)");
		MarsRover marsRover= new MarsRover(planetX,planetY,planetObstacles);
		
		assertEquals("(4,8,E)",marsRover.executeCommand(command));
		
	}

}
